from django.contrib.auth.hashers import make_password
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import ProtectedError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import View

from base.models import Section, Article, User, Role
from base.forms import CreateUserForm, EditUserForm, ArticleForm, CreateRoleForm


# Base class for all CMS views. Placed inside this class for scope purposes.
# Access checks may be written into a mixin later on, when there's more time.
class CmsView(View):
    login_url = '/login'
    raise_exception = False

    def check_section_access(self, section, user):
        return section in user.allowed_sections.all() or user.is_staff

    def check_users_access(self, user):
        return user.role.can_edit_staff_list or user.is_staff

    def get_allowed_sections(self, user):
        if user.is_staff:
            return Section.objects.all()
        else:
            return user.allowed_sections.all()


# Shows the first page of the CMS. Currently this contains an overview
# of the five latest articles within a section, but could later be
# user as a dashboard with graphs for traffic etc.
class HomeView(LoginRequiredMixin, CmsView):
    template = "home.html"

    def get(self, request):
        sections = []
        allowed_sections = self.get_allowed_sections(request.user)
        for section in Section.objects.all():
            if section not in allowed_sections:
                continue

            articles = Article.objects.filter(section=section)
            sections.append({
                'instance': section,
                'articles': articles[:5],
                'count': len(articles)
            })

        return render(request, self.template, {
            'sections': sections,
        })


# Allows users with the relevant permissions
# to create, edit and delete sections.
class SectionsView(LoginRequiredMixin, CmsView):
    template = 'sections.html'

    def get(self, request):
        if not request.user.role.can_edit_sections and not request.user.is_staff:
            return HttpResponseRedirect(reverse('cms'))

        return render(request, self.template, {
            'sections': Section.objects.all()
        })

    def post(self, request):
        if not request.user.role.can_edit_sections and not request.user.is_staff:
            return HttpResponseRedirect(reverse('cms'))

        if request.POST['action'] == "delete":
            for section in Section.objects.filter(pk__in=request.POST.getlist('section')):
                section.delete()
        elif request.POST['action'] == "add":
            Section.objects.create(
                name=request.POST['new_name'],
                hex_color=request.POST['hex_color'][1:]
            )

        return render(request, self.template, {
            'sections': Section.objects.all()
        })


# The page to show the full list of a single section.
# Includes a settings widget at the top of the page if the user
# has the right permissions.
class SectionView(LoginRequiredMixin, CmsView):
    template = 'section.html'

    def get(self, request, section_id=None):
        section = Section.objects.get(pk=section_id)

        return render(request, self.template, {
            'section': section,
            'articles': Article.objects.filter(section=section),
        })

    def post(self, request, section_id):
        section = Section.objects.get(pk=section_id)
        if not request.user.role.can_edit_sections:
            return HttpResponseRedirect(reverse('cms'))

        section.name = request.POST['name']
        section.hex_color = request.POST['colour'][1:]
        section.save()

        return render(request, self.template, {
            'section': section,
            'articles': Article.objects.filter(section=section),
        })


# Edit an article. Get this when clicking in any overview on an article title.
class EditArticleView(LoginRequiredMixin, CmsView):
    template = 'edit_article.html'

    def get(self, request, article_id):
        article = Article.objects.get(pk=article_id)
        if not self.check_section_access(article.section, request.user):
            return HttpResponseRedirect(reverse('cms'))

        form = ArticleForm(instance=article)
        form.set_sections(request.user.allowed_sections)
        return render(request, self.template, {
            'article_form': form.as_p()
        })

    def post(self, request, article_id):
        article = Article.objects.get(pk=article_id)
        if not self.check_section_access(article.section, request.user):
            return HttpResponseRedirect(reverse('cms'))

        if request.POST['action'] == "Delete":
            article.delete()
        else:
            form = ArticleForm(request.POST, instance=article)
            form.save()

        return HttpResponseRedirect(reverse('cms'))


# Create a new article. We're journalists now!
class AddArticleView(LoginRequiredMixin, CmsView):
    template = 'add_article.html'

    def get(self, request, section_id=None):
        # Usually people would always have a section_id, because it
        # is included with every "post new article" link, but *suppose*
        # someone would modify the url to have no section_id, we can
        # at least show the form but without the dropdown box
        # preselected.
        if section_id:
            section = Section.objects.get(pk=section_id)
            if not self.check_section_access(section, request.user):
                return HttpResponseRedirect(reverse('cms'))

            form = ArticleForm(initial={'section': section})
        else:
            form = ArticleForm()
        form.set_sections(self.get_allowed_sections(request.user))
        return render(request, self.template, {
            'article_form': form.as_p()
        })

    def post(self, request, section_id):
        # Surely nobody is going to do something crazy as modifying a POST
        # request to change the section an article is going to be posted
        # in, right? Surely not!
        if Section.objects.get(pk=section_id) in request.user.allowed_sections.all():
            form = ArticleForm(request.POST)
            form.save()

        return HttpResponseRedirect(reverse('cms'))


# Overview of the roles and the users that are not set to 'is_staff'.
class UserView(LoginRequiredMixin, CmsView):
    template = 'users.html'

    def get(self, request):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))

        return render(request, self.template, {
            'users': User.objects.filter(is_staff=False),
            'roles': Role.objects.all(),
            'error_msg': '',
        })

    # Editing and deleting Roles
    def post(self, request):
        error_msg = ''
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))

        if request.POST['action'] == "Save":
            role = Role.objects.get(pk=request.POST['role_id'])
            role.can_edit_sections = "sections" in request.POST
            role.can_edit_staff_list = "staff" in request.POST
            role.can_del_comments = "comments" in request.POST
            role.save()

        elif request.POST['action'] == "Delete":
            # Users are protected from deletion through cascades
            try:
                Role.objects.get(pk=request.POST['role_id']).delete()
            except ProtectedError:
                error_msg = "There are still users with this Role!"

        return render(request, self.template, {
            'users': User.objects.filter(is_staff=False),
            'roles': Role.objects.all(),
            'error_msg': error_msg,
        })


# A form to enter the bare minimum information to create a new user.
class AddUserView(LoginRequiredMixin, CmsView):
    template = 'add_user.html'

    def get(self, request):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))
        form = CreateUserForm()
        return render(request, self.template, {
            'user_form': form.as_p()
        })

    def post(self, request):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))

        sections = Section.objects.filter(id__in=request.POST.getlist('allowed_sections'))
        new_user = User.objects.create_user(
            username=request.POST['username'],
            password=request.POST['password'],
            role=Role.objects.get(pk=request.POST['role']),
        )
        new_user.allowed_sections.set(sections)
        new_user.save()
        return HttpResponseRedirect(reverse('users'))


# Enables users to enter some more info.
class EditUserView(LoginRequiredMixin, CmsView):
    template = 'edit_user.html'

    def get(self, request, user_id):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))

        form = EditUserForm(instance=User.objects.get(pk=user_id))

        return render(request, self.template, {
            'user_form': form.as_p()
        })

    def post(self, request, user_id):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))

        user = User.objects.get(pk=user_id)

        if request.POST['action'] == "Delete":
            user.delete()

        else:
            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']
            user.allowed_sections.set(Section.objects.filter(id__in=request.POST.getlist('allowed_sections')))
            user.role = Role.objects.get(pk=request.POST['role'])

            if not request.POST['password'].startswith('pbkdf2_sha256'):
                user.password = make_password(request.POST['password'])

            user.save()

        return HttpResponseRedirect(reverse('users'))


class AddRoleView(LoginRequiredMixin, CmsView):
    template = 'add_role.html'

    def get(self, request):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))
        form = CreateRoleForm()

        return render(request, self.template, {
            'role_form': form.as_p()
        })

    def post(self, request):
        if not self.check_users_access(request.user):
            return HttpResponseRedirect(reverse('cms'))
        form = CreateRoleForm(request.POST)
        form.save()
        return HttpResponseRedirect(reverse('users'))
