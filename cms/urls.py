from django.urls import path
from .views import HomeView, SectionsView, SectionView, AddArticleView, EditArticleView, \
    UserView, AddUserView, EditUserView, AddRoleView

urlpatterns = [
    path('', HomeView.as_view(), name='cms'),
    path('section/<int:section_id>', SectionView.as_view(), name='section'),
    path('section', SectionsView.as_view(), name='sections'),
    path('article/<int:article_id>', EditArticleView.as_view(), name='edit_article'),
    path('article/new', AddArticleView.as_view()),
    path('article/new/<int:section_id>', AddArticleView.as_view(), name='add_article'),
    path('user', UserView.as_view(), name='users'),
    path('user/new', AddUserView.as_view(), name='add_user'),
    path('user/<int:user_id>', EditUserView.as_view(), name='edit_user'),
    path('user/role', AddRoleView.as_view(), name='add_role')
]
