from django import template
from base.models import Section

register = template.Library()


@register.inclusion_tag('templatetags/section_list.html')
def section_list():
    return {'sections': Section.objects.all()}
