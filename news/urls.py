from django.urls import path
from .views import IndexView, SectionAjax, ArticleAjax

urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('section', SectionAjax.as_view(), name='news_section'),
    path('article/<int:article_id>', ArticleAjax.as_view(), name='news_article'),
]
