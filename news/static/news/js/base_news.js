// Put listeners on all navigation buttons in the left hand box.
let sectionLinks = document.querySelectorAll('.section_link');

for (let i=0; i<sectionLinks.length; i++) {
    sectionLinks[i].addEventListener('click', function () {
        fetchArticles(sectionLinks[i].getAttribute('data-section_id'));
    });
}
