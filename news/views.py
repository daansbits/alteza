import json
from django.shortcuts import render
from django.views import View
from base.models import Article, Section, Comment


class IndexView(View):
    template = 'index.html'

    def get(self, request):
        return render(request, self.template, {
            'articles': Article.objects.filter(is_visible=True).order_by('-creation_time')
        })


class SectionAjax(View):
    template = 'ajax/news_section.html'

    def post(self, request):
        data = json.loads(request.body)
        if data['section'] == '0':
            articles = Article.objects.all()
        else:
            articles = Article.objects.filter(section=Section.objects.get(pk=data['section']))

        return render(request, self.template, {
            'articles': articles
        })


class ArticleAjax(View):
    template = 'news_article.html'

    def fetch_comments(self, article, user):
        comments = Comment.objects.filter(article=article).order_by('-pk')
        if not hasattr(user, 'role') or not user.role.can_del_comments:
            comments = comments.filter(is_hidden=False)
        return comments

    def get(self, request, article_id):
        article = Article.objects.get(pk=article_id)

        return render(request, self.template, {
            'article': article,
            'comments': self.fetch_comments(article, request.user),
            'allow_dels': (hasattr(request.user,
                                   'role') and request.user.role.can_del_comments) or request.user.is_staff
        })

    def post(self, request):
        article = Article.objects.get(pk=request.POST['section'])

        if request.POST['action'] == "Post":
            Comment.objects.create(
                article=article,
                content=request.POST['new_comment'],
            )

        # Comments don't truly get deleted; if you have someone who misbehaves,
        # you may want to find out more or gather some "evidence".
        elif request.POST['action'] == "Delete":
            comment = Comment.objects.get(pk=request.POST['comment_id'])
            comment.is_hidden = True
            comment.save()

        return render(request, self.template, {
            'article': article,
            'comments': self.fetch_comments(article, request.user),
            'allow_dels': (hasattr(request.user,
                                   'role') and request.user.role.can_del_comments) or request.user.is_staff
        })
