De nieuwswebsite is opgedeeld in categorieën die via het CMS kunnen worden gewijzigd.

Om bij het CMS te komen, log in met één van onderstaande accounts en klik op de hoofdsite in de rechterbovenhoek op 'CMS'.

CMS-gebruikers hebben enkel toegang tot de nieuwscategorieën en functionaliteiten waar zij aan zijn toegewezen, tenzij ze ook "staff" zijn en toegang hebben tot het Django Admin CP. In zulke gevallen zou een deel van het CMS afsluiten vrij nutteloos zijn.

Als ze ook comments kunnen verwijderen, doen ze dat op de site zelf: bij elke comment staat een "delete"-knop waarmee comments verborgen kunnen worden. Ze zullen dan niet écht verwijderd zijn voor toekomstige referentie.

### Inloggegevens

Accountgegevens superuser: admin/admin

Verdere gebruikers: 
- rmoerland/nieuws (Media, administratie van categorieën en gebruikers)
- grossum/python (Software, Politiek)
- jhaartsen/blauwtand (Hardware, Media)

### Contact

Daan Arend  
Email: contact at omniadicta dot net  
