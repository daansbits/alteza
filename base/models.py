from django.contrib.auth.models import AbstractUser
from django.db import models


class Article(models.Model):
    section = models.ForeignKey('Section', null=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=200)
    body = models.TextField()
    is_visible = models.BooleanField(default=True)
    creation_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Section(models.Model):
    name = models.CharField(max_length=50)
    hex_color = models.CharField(max_length=6, default="FFFFFF")

    def __str__(self):
        return self.name


class Role(models.Model):
    name = models.CharField(max_length=100)
    can_edit_staff_list = models.BooleanField(default=False)
    can_edit_sections = models.BooleanField(default=False)
    can_del_comments = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class User(AbstractUser):
    allowed_sections = models.ManyToManyField(Section, blank=True)
    role = models.ForeignKey(Role, on_delete=models.PROTECT)

    def __str__(self):
        return self.username


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    content = models.TextField()
    is_hidden = models.BooleanField(default=False)

    def __str__(self):
        return self.article.title[:15] + " - " + self.content[:50]
