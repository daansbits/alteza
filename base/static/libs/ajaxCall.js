function fetchPage(url, target_div, data) {
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken'),
        },
        mode: 'same-origin',
        body: JSON.stringify(data)
    }).then(function (response) {
        return response.text();
    }).then(function(html) {
        target_div.innerHTML = html;
    });
}