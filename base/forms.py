from django.forms import ModelForm, CheckboxSelectMultiple

from base.models import User, Article, Role


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        exclude = []

    # When a CMS users creates a new article, they should only have be
    # able to select the sections they have access to from the
    # drop down menu.
    def set_sections(self, sections):
        self.fields['section'].queryset = sections


class CreateUserForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'role', 'allowed_sections']
        widgets = {'allowed_sections': CheckboxSelectMultiple, }


class EditUserForm(ModelForm):
    class Meta:
        model = User
        fields = ['password', 'role', 'allowed_sections', 'first_name', 'last_name']
        widgets = {'allowed_sections': CheckboxSelectMultiple, }


class CreateRoleForm(ModelForm):
    class Meta:
        model = Role
        exclude = ()
