from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.apps import apps
from django.conf import settings

for app in settings.ADDITIONAL_APPS:
    for model in apps.get_app_config(app).get_models():
        admin.site.register(model)
