from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import View


class LoginView(View):
    template = 'login.html'

    def get(self, request):
        if request.user.is_authenticated:
            # Logged in users have no business being here
            return HttpResponseRedirect(reverse('home'))
        else:
            return render(request, self.template, {'message': ''})

    def post(self, request):
        user = authenticate(username=request.POST['user'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('home'))
        else:
            # Show login page again, add an error message
            return render(request, self.template, {'message': 'Incorrect credentials entered.'})


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('home'))
